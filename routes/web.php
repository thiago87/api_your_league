<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('api/v1/users/login', 'UsersController@login');
$router->group(['namespace' => '\Rap2hpoutre\LaravelLogViewer'], function() use ($router) {
    $router->get('api/v1/logs', 'LogViewerController@index');
});

$router->group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], function () use ($router) {

	/**
	 * Routes for resource users
	 */
	$router->get('users', 'UsersController@all');
	$router->get('users/{id}', 'UsersController@get');
	$router->get('users/points_by_user/{id}', 'UsersController@points_by_user');
	$router->get('users/points_by_user/{id}/{session}', 'UsersController@points_by_user');
	$router->post('users', 'UsersController@add');
	$router->put('users/{id}', 'UsersController@put');
	$router->delete('users/{id}', 'UsersController@remove');

	/**
	 * Routes for resource groups
	 */
	$router->get('groups', 'GroupsController@all');
	$router->get('groups/{id}', 'GroupsController@get');
	$router->post('groups', 'GroupsController@add');
	$router->put('groups/{id}', 'GroupsController@put');
	$router->delete('groups/{id}', 'GroupsController@remove');

	/**
	 * Routes for resource users groups
	 */
	$router->post('users_groups', 'UsersGroupsController@add');
	$router->put('users_groups/add_payment/{id}', 'UsersGroupsController@add_payment');
	$router->put('users_groups/remove_payment/{id}', 'UsersGroupsController@remove_payment');
	$router->delete('users_groups/remove_time/{groupId}/{timeId}', 'UsersGroupsController@remove_time');


	/**
	 * Routes for resource players
	 */
	$router->get('players', 'PlayersController@all');
	$router->get('players/{id}', 'PlayersController@get');
	// $router->get('players/population', 'PlayersController@population');
	$router->post('players', 'PlayersController@add');
	$router->put('players/{id}', 'PlayersController@put');
	$router->delete('players/{id}', 'PlayersController@remove');

	/**
	 * Routes for resource group_types
	 */
	$router->get('group_types/active', 'GroupTypesController@active');
	$router->get('group_types', 'GroupTypesController@all');
	$router->get('group_types/{id}', 'GroupTypesController@get');
	// $router->post('group_types', 'GroupTypesController@add');
	// $router->put('group_types/{id}', 'GroupTypesController@put');
	// $router->delete('group_types/{id}', 'GroupTypesController@remove');

	/**
	 * Routes for resource user-points
	 */
	$router->get('user_points/refresh_session/{session}', 'UserPointsController@refresh_session');
	// $router->get('user-points', 'UserPointsController@all');
	// $router->get('user-points/{id}', 'UserPointsController@get');
	// $router->post('user-points', 'UserPointsController@add');
	// $router->put('user-points/{id}', 'UserPointsController@put');
	// $router->delete('user-points/{id}', 'UserPointsController@remove');

});
