<?php

use Carbon\Carbon;

class GroupTest extends TestCase {

    public function testApplication() {
        $user = factory('App\Users')->create();

        $this->post('/api/v1/users/login', ['email'=>$user->email, 'password'=>$user->password])->seeJson(
            [
                'success' => true
            ]
            );

            $token = json_decode($this->response->getContent())->data->jwt;

            $this->get('/api/groups', ['Authorization' => $token])
                ->seeJson(['message' => 'authenticated_user']);
    }

    public function testCrateGroup() {
        $carbon = new Carbon();
        // $this->post('api/v1/groups', ['name' => 'Group 1', 'price' => 20, 'group_type_id' => 1, 'payment_limit' => $carbon->create()->setTimezone('utc')])
        //     ->seeJsonEquals([
        //         'created' => true,
        //     ]);
        $this->assertTrue(true);
    }

}