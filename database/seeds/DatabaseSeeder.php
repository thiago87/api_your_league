<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = Carbon::now()->toDateTimeString();

        DB::table('group_types')->insert([
            [
                'name' => 'Geral',
                'description' => 'Premiação para campeão geral',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 1,
                'finished_session' => 38,
                'is_active' => 0
            ],
            [
                'name' => '1º Turno',
                'description' => 'Premiação para campeão do primeiro turno',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 1,
                'finished_session' => 19,
                'is_active' => 0
            ],
            [
                'name' => '2º Turno',
                'description' => 'Premiação para campeão do segundo turno',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 20,
                'finished_session' => 38,
                'is_active' => 1
            ],
            [
                'name' => 'Abril',
                'description' => 'Todas as rodadas de abril',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 1,
                'finished_session' => 1,
                'is_active' => 0
            ],
            [
                'name' => 'Maio',
                'description' => 'Todas as rodadas de maio',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 2,
                'finished_session' => 6,
                'is_active' => 0
            ],
            [
                'name' => 'Junho',
                'description' => 'Todas as rodadas de Junho',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 7,
                'finished_session' => 9,
                'is_active' => 0
            ],
            [
                'name' => 'Julho',
                'description' => 'Todas as rodadas de Julho',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 10,
                'finished_session' => 12,
                'is_active' => 0
            ],
            [
                'name' => 'Agosto',
                'description' => 'Todas as rodadas de Agosto',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 13,
                'finished_session' => 16,
                'is_active' => 1
            ],
            [
                'name' => 'Setembro',
                'description' => 'Todas as rodadas de Setembro',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 17,
                'finished_session' => 22,
                'is_active' => 1
            ],
            [
                'name' => 'Outubro',
                'description' => 'Todas as rodadas de Outubro',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 23,
                'finished_session' => 29,
                'is_active' => 1
            ],
            [
                'name' => 'Novembro',
                'description' => 'Todas as rodadas de Novembro',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 30,
                'finished_session' => 35,
                'is_active' => 1
            ],
            [
                'name' => 'Dezembro',
                'description' => 'Todas as rodadas de Dezembro',
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'started_session' => 36,
                'finished_session' => 38,
                'is_active' => 1
            ]
        ]);
    }
}
