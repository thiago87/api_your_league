<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('email_cartolafc');
            $table->string('password');
            $table->string('password_cartolafc');
            $table->boolean('confirm_email');
            $table->longText('token');
            $table->integer('time_id');
            $table->text('foto_perfil');
            $table->string('nome_time');
            $table->string('slug');
            // Constraints declaration
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
