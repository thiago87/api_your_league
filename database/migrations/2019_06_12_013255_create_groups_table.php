<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{

    public function up()
    {
        Schema::create('groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('cartola_group_id')->nullable();
            $table->string('image')->nullable();
            $table->string('link_invitation');
            $table->float('price')->nullable();
            $table->integer('group_type_id');
            $table->date('payment_limit')->nullable();
            // Constraints declaration
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('groups');
    }
}
