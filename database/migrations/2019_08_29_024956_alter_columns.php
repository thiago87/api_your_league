<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('email_cartolafc')->nullable()->change();
            $table->string('password')->nullable()->change();
            $table->string('password_cartolafc')->nullable()->change();
            $table->longText('token')->nullable()->change();
            $table->integer('time_id')->nullable()->change();
            $table->text('foto_perfil')->nullable()->change();
            $table->string('nome_time')->nullable()->change();
            $table->string('slug')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
