<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{

    public function up()
    {
        Schema::create('players', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nickname');
            $table->string('foto')->nullable();
            $table->string('slug');
            $table->integer('cartola_player_id');
            $table->integer('position_id');
            $table->float('price');
            $table->float('average');
            $table->integer('play_quantity');
            $table->json('scout');
            // Constraints declaration
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('players');
    }
}
