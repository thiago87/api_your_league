<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddStartedAndFinishedSessionToGroupTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_types', function (Blueprint $table) {
            $table->integer('started_session');
            $table->integer('finished_session');
        });

        // Add start to save groups
        $typeOne = DB::table('group_types')
        ->where('id', 1)
        ->update(['started_session' => 1, 'finished_session' => 38]);
        $typeTwo = DB::table('group_types')
        ->where('id', 2)
        ->update(['started_session' => 1, 'finished_session' => 19]);
        $typeTrhee = DB::table('group_types')
        ->where('id', 3)
        ->update(['started_session' => 20, 'finished_session' => 38]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_types', function (Blueprint $table) {
            $table->dropColumn(['started_session', 'finished_session']);
        });
    }
}
