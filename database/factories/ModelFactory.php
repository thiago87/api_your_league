<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

/**
 * Factory definition for model App\Users.
 */
$factory->define(App\Users::class, function ($faker) {
    return [
        'name' => 'Thiago Freitas',
        'email' => 'thiagofreitas_659@hotmail.com',
        'email_cartolafc' => 'thiagofreitas_659@hotmail.com',
        'password' => 'atletico',
        'password_cartolafc' => 'atletico',
        'confirm_email' => true,
        'time_id' => 110014,
        'nome_time' => 'trfreitasCAP',
        'slug' => 'trfreitascap'
    ];
});

/**
 * Factory definition for model App\Groups.
 */
$factory->define(App\Groups::class, function ($faker) {
    return [
        'name' => $faker->word,
        'link_invitation' => $faker->slug,
        'price' => $faker->randomFloat(3, 0, 100),
        'group_type_id' => 1,
        'started_session' => $faker->numberBetween(1, 38),
        'finished_session' => $faker->numberBetween(1, 38)
    ];
});

/**
 * Factory definition for model App\Players.
 */
$factory->define(App\Players::class, function ($faker) {
    return [
        // Fields here
    ];
});

/**
 * Factory definition for model App\UserPoints.
 */
$factory->define(App\UserPoints::class, function ($faker) {
    return [
        // Fields here
    ];
});
