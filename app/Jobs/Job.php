<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ixudra\Curl\Facades\Curl;

abstract class Job implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "queueOn" and "delay" queue helper methods.
    |
    */

    use InteractsWithQueue, Queueable, SerializesModels;

    protected function getApiCartola(String $url) {
        // Pega a rodada atual
        return Curl::to(ENV('URL_CARTOLA').$url)
            ->returnResponseObject()
            ->asJson(true)
            ->withOption( 'USERAGENT', 'spider' )
            ->withHeader('Content-Type: application/json')
            ->get();
    }
}
