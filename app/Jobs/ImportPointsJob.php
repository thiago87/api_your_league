<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Players;
use App\Users;

class ImportPointsJob extends Job
{

    protected $user;
    protected $start_session;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user = null, $start_session = null)
    {
        $this->user = $user;
        if($start_session === null ){
            $this->start_session = 1;
        } else {
            $this->start_session = $start_session;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $status_mercado = $this->getApiCartola("mercado/status");

        // $status_mercado = json_decode($status_mercado);
        if($status_mercado && $status_mercado->content) {
            $rodada_atual = $status_mercado->content['rodada_atual'];

            // Quando mercado esta aberto, pego apenas o time das rodadas passadas
            if($status_mercado->content['status_mercado']) {
                $rodada_atual = $rodada_atual - 1;
            }

            $teams = [];
            if($this->user['time_id'] !== null) {
                $teams = [(object) $this->user];
            } else {
                $users = new Users;
                $teams = $users->all();
            }
            $timestamp = Carbon::now()->toDateTimeString();
            $user_points = [];
            // Sincronizo as rodadas passada
            foreach($teams as $key => $team) {
                for($i = $this->start_session; $i <= $rodada_atual; $i++) {
                    $points = $this->getApiCartola("time/id/$team->time_id/$i");
                    $captain_id = $points->content['capitao_id'];

                    // Adiciona
                    foreach ($points->content['atletas'] as $key => $atleta) {
                        $user_points[] = array(
                            'user_id' => $team->id,
                            'player_id' => $atleta['atleta_id'],
                            'points' => $atleta['pontos_num'],
                            'session' => $atleta['rodada_id'],
                            'is_captain' => ($atleta['atleta_id'] == $captain_id ? true : false),
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        );

                        if(DB::table('players')->where('cartola_player_id', $atleta['atleta_id'])->doesntExist()) {
                            $player = new Players;

                            $player->name = $atleta['nome'];
                            $player->nickname = $atleta['apelido'];
                            $player->foto = $atleta['foto'];
                            $player->slug = $atleta['slug'];
                            $player->cartola_player_id = $atleta['atleta_id'];
                            $player->position_id = $atleta['posicao_id'];
                            $player->price = $atleta['preco_num'];
                            $player->play_quantity = $atleta['jogos_num'];
                            $player->average = $atleta['media_num'];
                            $player->scout = json_encode($atleta['scout']);

                            $player->save();
                        }
                    }
                }
            }

            if($user_points) {
                DB::table('user_points')->insert($user_points);
            }
        }

        return true;
    }
}