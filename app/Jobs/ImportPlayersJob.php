<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use App\Players;

class ImportPlayersJob extends Job
{

    protected $player_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($player_id = null)
    {
        //
        $this->player_id = $player_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = ENV('URL_CARTOLA');

        // Pega a rodada atual
        $status_mercado = Curl::to($url."mercado/status")
            ->returnResponseObject()
            ->asJson(true)
            ->withOption( 'USERAGENT', 'spider' )
            ->withHeader('Content-Type: application/json')
            ->get();

        if($status_mercado && $status_mercado->content && $status_mercado->content['status_mercado'] == 1) {
            $players = Curl::to($url."atletas/mercado")
                ->returnResponseObject()
                ->asJson(true)
                ->withOption( 'USERAGENT', 'spider' )
                ->withHeader('Content-Type: application/json')
                ->get();

            if($players && $players->content && $players->content['atletas']) {
                foreach($players->content['atletas'] as $key => $value) {
                    $player = new Players;

                    $player->name = $value['nome'];
                    $player->nickname = $value['apelido'];
                    $player->foto = $value['foto'];
                    $player->slug = $value['slug'];
                    $player->cartola_player_id = $value['atleta_id'];
                    $player->position_id = $value['posicao_id'];
                    $player->price = $value['preco_num'];
                    $player->play_quantity = $value['jogos_num'];
                    $player->average = $value['media_num'];
                    $player->scout = json_encode($value['scout']);

                    $player->save();
                }
            }
        }
    }
}