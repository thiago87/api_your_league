<?php

namespace App;

use App\Jobs\ImportPointsJob;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model {
    use SoftDeletes;

    protected $fillable = ['email', 'email_cartolafc', 'password', 'password_cartolafc'];
    protected $dates = [];
    protected $hidden = ['password', 'password_cartolafc'];

    public $logged = null;

    public function scopeWithAll($query)
    {
        $query;
    }

    public function save(array $options = [])
    {
        // before save code
        $id = $this->id;

        $is_save = parent::save($options);

        // after save
        if($is_save && !$id) {
            $this->importPoints();
        }
    }

    public function getJwt() {
        return $jwt = [
            'iss' => "your-league", // Issuer of the token
            'sub' => $this->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];
    }

    // Relationships

    // Callbacks
    protected function importPoints() {
        $job = (new ImportPointsJob($this->attributes));
        dispatch($job);
    }

}
