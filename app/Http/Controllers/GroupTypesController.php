<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;

class GroupTypesController extends Controller {

    const MODEL = "App\GroupTypes";

    use RESTActions;

    public function active() {
        $m = self::MODEL;
        $m = $m::where('is_active', true);
        return $this->respond(Response::HTTP_OK, $m->withAll()->get());
    }
}
