<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Ixudra\Curl\Facades\Curl;

class Controller extends BaseController
{

    protected function sendRequest($path, $login = false) {
        $url = ENV('URL_CARTOLA');
        if($login) {
            $url = 'https://login.globo.com/api/authentication';
        } else {
            $url .= $path;
        }

        return Curl::to($url)
            ->returnResponseObject()
            ->asJson(true)
            ->withOption( 'USERAGENT', 'spider' )
            ->withHeader('Content-Type: application/json');
    }

}
