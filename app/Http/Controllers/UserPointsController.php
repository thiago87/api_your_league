<?php namespace App\Http\Controllers;

use App\Jobs\ImportPointsJob;

class UserPointsController extends Controller {

    const MODEL = "App\UserPoints";

    use RESTActions;

    public function refresh_session($session) {
        if (!$session) {
            return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Erro desconhecido')));
        }
        $job = (new ImportPointsJob(null, $session));
        $this->dispatch($job);

        exit();
    }

}
