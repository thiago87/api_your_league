<?php namespace App\Http\Controllers;

use Log;
use App\Users;
use App\Groups;
use App\UsersGroups;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UsersGroupsController extends Controller {

    use RESTActions;

    public function add(Request $request) {
        $group = new Groups();
        $user = new Users();
        $group = $group->find($request->input('group_id'));

        if(!$group) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        DB::beginTransaction();

        $time_id = $request->input('time_id');
        $user = $user->where('time_id', $time_id)->first();
        if (!$user) {
            $response = $this->sendRequest('time/id/'.$time_id)
                    ->get();
            if($response && $response->status == 200 && $response->content['time']) {

                $user = new Users();

                $user->confirm_email = false;
                $user->time_id = $response->content['time']['time_id'];
                $user->foto_perfil = $response->content['time']['foto_perfil'];
                $user->nome_time = $response->content['time']['nome'];
                $user->name = $response->content['time']['nome_cartola'];
                $user->slug = $response->content['time']['slug'];
                $user->save();

                if ($user->id) {
                    $userGroups = new UsersGroups;
                    $userGroups->user_id = $user->id;
                    $userGroups->group_id = $group->id;
                    $userGroups->save();
                    if (!$userGroups->id) {
                        DB::rollBack();
                        Log::error(__FILE__.":".__LINE__);
                    }
                } else {
                    DB::rollBack();
                    Log::error(__FILE__.":".__LINE__);
                }
            } else {
                DB::rollBack();
                Log::error(__FILE__.":".__LINE__);
            }
        } else {
            $userGroups = new UsersGroups;
            $userGroups->user_id = $user->id;
            $userGroups->group_id = $group->id;
            if (!$userGroups->save()) {
                DB::rollBack();
                Log::error(__FILE__.":".__LINE__);
            }
        }

        DB::commit();

        return $this->respond(Response::HTTP_OK, array('data'=>$user, 'success'=>true));
    }

    public function remove_time($groupId, $timeId) {
        $user = new Users();
        $user = $user->where('time_id', $timeId)->first();
        if(!$user) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        $usersGroups = new UsersGroups();
        $usersGroups = $usersGroups->where(array('group_id' => $groupId, 'user_id' => $user->id))->first();
        if(!$usersGroups) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        $usersGroups->delete();
        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    public function add_payment($id, Request $request) {
        $userGroup = new UsersGroups();
        $userGroup = $userGroup->where(array('id' => $id, 'user_id' => $request->input('user_id'), 'group_id' => $request->input('group_id')))->first();

        if(!$userGroup) {
            Log::error(__FILE__.":".__LINE__);
            return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Grupo ')));
        }

        $userGroup->update(['has_paid' => true]);
        return $this->respond(Response::HTTP_OK);

    }

    public function remove_payment($id, Request $request) {
        $userGroup = new UsersGroups();
        $userGroup = $userGroup->where(array('id' => $id, 'user_id' => $request->input('user_id'), 'group_id' => $request->input('group_id')))->first();

        if(!$userGroup) {
            Log::error(__FILE__.":".__LINE__);
            return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Grupo ')));
        }

        $userGroup->update(['has_paid' => 0]);
        return $this->respond(Response::HTTP_OK);

    }
}