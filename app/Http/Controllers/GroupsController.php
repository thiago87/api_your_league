<?php namespace App\Http\Controllers;

use Log;
use App\GroupAwards;
use App\Users;
use App\UserPoints;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GroupsController extends Controller {

    const MODEL = "App\Groups";

    use RESTActions;

    // public function get($id)
    // {
    //     $m = self::MODEL;
    //     $model = $m::find($id);
    //     if(is_null($model)){
    //         return $this->respond(Response::HTTP_NOT_FOUND);
    //     }

    //     foreach($model->users_groups as $key => &$user_group) {
    //         $user_group['total_points'] = $this->sumUserGroupPoints($user_group->user_id, $model->group_type['started_session'], $model->group_type['finished_session']);
    //     }
    //     return $this->respond(Response::HTTP_OK, $model);
    // }

    public function add(Request $request)
    {
        $m = self::MODEL;
        $request->merge(['price' => $this->formatMoney($request->price)]);
        $this->validate($request, $m::$rules);
        try {
            $save = $m::create($request->all());
            if($save) {
                if(isset($request->awards) && !empty($request->awards)) {
                    $this->addAwardsToGroup($request->awards, $save->id);
                }
            }
            return $this->respond(Response::HTTP_CREATED, $save);
        } catch (Exception $e) {
            Log::error("[".__FILE__.":".__LINE__."] ".$e->getMessage());
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, $save);
        }
    }

    public function put(Request $request, $id)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        try {
            $data = $request->all();
            $awards = $data['awards'];
            unset($data['awards']);
            $model->update($data);
            if($id) {
                if(isset($awards) && !empty($awards)) {
                    $this->addAwardsToGroup($awards, $id);
                }
            }
            $model = $m::find($id);
            return $this->respond(Response::HTTP_OK, $model);
        } catch (Exception $e) {
            Log::error("[".__FILE__.":".__LINE__."] ".$e->getMessage());
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function addAwardsToGroup(Array $awards, $group_id) {
        $groupAward = new GroupAwards;
        $groupAward->where('group_id', $group_id)->delete();
        foreach ($awards as $key => $award) {
            $groupAward = new GroupAwards;
            $groupAward->position = ($key+1);
            $groupAward->price = $this->formatMoney($award);
            $groupAward->group_id = $group_id;
            $groupAward->save();
        }

    }

    protected function formatMoney($value) {
        return str_replace(",", ".", preg_replace('/[^0-9,]/', "", $value));
    }

    private function sumUserGroupPoints($user_id, $started, $finished) {
        $user_points = new UserPoints;
        return $user_points->where('user_id', $user_id)->whereBetween('session', [$started, $finished])->sum('points');
    }

}
