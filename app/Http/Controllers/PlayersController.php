<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Jobs\ImportPlayersJob;

class PlayersController extends Controller {

    const MODEL = "App\Players";

    use RESTActions;

    public function get_by_session($session)
    {
        $m = self::MODEL;
        $data = $m::where('session', '=', $session)->ordered();
        return response()->json($data, Response::HTTP_OK);
    }

    public function population() {

        $job = (new ImportPlayersJob());
		$this->dispatchNow($job);
    }

}
