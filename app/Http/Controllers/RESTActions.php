<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait RESTActions {


    public function all(Request $request)
    {
        $m = $this->pagination(self::MODEL, $request);
        if($request->get('filters')) {
            $filters = json_decode($request->get('filters'));
            $m = $this->applyFilter($m, $filters);
        }
        if($request->get('query')) {
            $m = $m->buildQuery($request->get('query'));
        }
        return $this->respond(Response::HTTP_OK, $m->get());
    }

    public function get($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    public function add(Request $request)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        return $this->respond(Response::HTTP_CREATED, $m::create($request->all()));
    }

    public function put(Request $request, $id)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        $model->update($request->all());
        return $this->respond(Response::HTTP_OK, $model);
    }

    public function remove($id)
    {
        $m = self::MODEL;
        if(is_null($m::find($id))){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        $m::destroy($id);
        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    protected function pagination($model, $request){
        $limit = 25;
        $page = 0;

        if($request->get('page') && is_numeric($request->get('page'))) {
            $page = $request->get('page');
        }

        if($request->get('limit') && is_numeric($request->get('limit'))) {
            $limit = $request->get('limit');
        }
        $model = new $model;
        $model = $model->select($model->getTable().".*");
        return $model->take($limit)->skip($page);
    }

    protected function respond($status, $data = [])
    {
        return response()->json($data, $status);
    }

    protected function applyFilter($model, Array $filters) {
        foreach ($filters as $key => $filter) {
            if(isset($filter->join)) {
                $join = $filter->join;
                $model = $model->join($join->table, $join->primary_key, $join->operator, $join->foreign_key);
            }
            $model = $model->where($filter->property, $filter->operator, $filter->value);
        }

        return $model;
    }

}
