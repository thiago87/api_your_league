<?php
namespace App\Http\Controllers;

use Log;
use App\Users;
use App\UserPoints;
use App\Jobs\ImportPointsJob;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

    const MODEL = "App\Users";

    use RESTActions;

    public function get($id) {
        // $m = self::MODEL;
        // $user = $m::find($id);
        $job = (new ImportPointsJob());
        $this->dispatch($job);

        exit();
    }

    public function points_by_user($id, $session = null) {
        $m = self::MODEL;
        $user = $m::find($id);
        $user_points = new UserPoints;

        if($session == null) {
            $session = $user_points->max('session');
        }
        $points = $user_points->where('session', $session)->where('user_id', $id)->with('player')->get();

        return $this->respond(Response::HTTP_OK, $points);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required',
            'password'  => 'required'
        ]);

        $email = $request->email;
        $password = $request->password;

        $user_instance = new Users;

        // Verifica se tem em nosso banco de dados
        $user = $user_instance
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        if($user) {
            $response = $this->login_cartola($email, $password);
            if($response && $response->status == 200) {
                $user->token = $response->content['glbId'];

                $user->save();
            } else {
                Log::error(__FILE__.":".__LINE__);
                return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Erro desconhecido')));
            }

        } else {

            // se não tem, cadastra e faz login com o cartola e retorna a chave
            $response = $this->login_cartola($email, $password);

            if($response && $response->status == 200) {

                $resp = $this->sendRequest('auth/time')
                    ->withHeader('X-GLB-Token: '.$response->content['glbId'])
                    ->get();

                if($resp && $resp->status == 200) {


                    $user = $user_instance
                            ->where('time_id', $resp->content['time']['time_id'])
                            ->first();

                    if($user) {
                        $user->update([
                            'email'=>$email,
                            'email_cartolafc'=>$email,
                            'password'=>$password,
                            'password_cartola_fc'=>$password
                        ]);
                    } else {
                        $user = $user_instance;

                        $user->token = $response->content['glbId'];
                        $user->email = $email;
                        $user->email_cartolafc = $email;
                        $user->password = $password;
                        $user->password_cartolafc = $password;
                        $user->confirm_email = true;
                        $user->time_id = $resp->content['time']['time_id'];
                        $user->foto_perfil = $resp->content['time']['foto_perfil'];
                        $user->nome_time = $resp->content['time']['nome'];
                        $user->name = $resp->content['time']['nome_cartola'];
                        $user->slug = $resp->content['time']['slug'];

                        $user->save();
                    }
                } else {
                    Log::error(__FILE__.":".__LINE__);
                    return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Erro desconhecido')));
                }
            } else {
                Log::error(__FILE__.":".__LINE__);
                return $this->respond(Response::HTTP_NOT_FOUND, array('data'=>array(), 'success'=>false, 'error'=>array('Erro desconhecido')));
            }
        }
        $user->jwt = $this->jwt($user);
        return $this->respond(Response::HTTP_OK, array('data'=>$user, 'success'=>true));
    }

    protected function jwt(Users $user) {
        $jwt = $user->getJwt();

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($jwt, env('JWT_SECRET'));
    }

    private function login_cartola($email, $password) {

        $response = $this->sendRequest('', true)
            ->withData(
                array('payload' =>
                    array(
                        'email' => $email,
                        'password' => $password,
                        'serviceId' => 4728
                    )
                )
            )
            ->post();

        return $response;
    }

}
