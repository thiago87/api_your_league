<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupTypes extends Model {

    use SoftDeletes;

    public static $GENERAL = 1;
    public static $FIRST_SESSION = 2;
    public static $SECOND_SESSION = 3;
    public static $APRIL_SESSION = 4;
    public static $MAY_SESSION = 5;
    public static $JUNE_SESSION = 6;
    public static $JULY_SESSION = 7;
    public static $AUGUST_SESSION = 8;
    public static $SEPTEMBER_SESSION = 9;
    public static $OCTOBER_SESSION = 10;
    public static $NOVEMBER_SESSION = 11;
    public static $DECEMBER_SESSION = 12;

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        "name" => "required",
    ];

    public function scopeWithAll($query)
    {
        $query;
    }

    // Relationships
    public function groups()
    {
        return $this->hasMany('App\Groups');
    }
}
