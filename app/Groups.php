<?php namespace App;

use App\UsersGroups;
use App\GroupTypes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Middleware\JwtAuthenticate;
use App\Scopes\GroupScope;

class Groups extends Model {

    use SoftDeletes;

    protected $fillable = ['name', 'price', 'group_type_id', 'payment_limit'];
    protected $dates = [];

    public static $rules = [
        "name" => "required",
    ];
    protected $with = ['users', 'group_type', 'group_awards', 'users_groups'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new GroupScope);
    }

    public function save(array $options = [])
    {
        // For verify create or edit
        $id = $this->id;
        $carbon = new Carbon();
        $this->payment_limit = $carbon->create($this->payment_limit)->setTimezone('utc');

        // before save
        $this->link_invitation = str_slug($this->name, "-");
        $this->fillRangeSession();

        $is_save = parent::save($options);

        // after save code
        if($is_save && !$id) {
            $this->addUserToGroup();
        }
    }

    // Relationships
    public function group_type()
    {
        return $this->belongsTo('App\GroupTypes');
    }

    public function users()
    {
        return $this->belongsToMany('App\Users', 'users_groups', 'group_id', 'user_id')->where('users.deleted_at', null)->where('users_groups.deleted_at', null);
    }

    public function group_awards()
    {
        return $this->hasMany('App\GroupAwards', 'group_id');
    }

    public function users_groups()
    {
        return $this->hasMany('App\UsersGroups', 'group_id')->where('deleted_at', null);
    }

    private function fillRangeSession() {
        $groupType = new GroupTypes();
        $groupType = $groupType->find($this->group_type_id);
        $this->started_session = $groupType->started_session;
        $this->finished_session = $groupType->finished_session;
    }

    private function addUserToGroup() {
        $user = JwtAuthenticate::getLoggedUser();
        $usersGroups = new UsersGroups;
        $usersGroups->user_id = $user['id'];
        $usersGroups->group_id = $this->id;
        $usersGroups->is_admin = true;
        $usersGroups->has_paid = false;
        $usersGroups->save();
    }

}
