<?php namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Middleware\JwtAuthenticate;

class GroupScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $user = JwtAuthenticate::getLoggedUser();
        $builder->select('groups.*')->join('users_groups', 'groups.id', '=', 'users_groups.group_id')->where('users_groups.user_id', $user['id']);
    }
}