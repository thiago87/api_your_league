<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Players extends Model {
    use SoftDeletes;

    protected $fillable = [];

    protected $dates = [];
    protected $appends = ['position_name'];

    public static $rules = [
        "name" => "required",
    ];

    public function scopeOrdered($model)
	{
	    return $model->orderBy('session', 'asc')->orderBy('position_id', 'asc');
    }

    public function scopebuildQuery($model, $query) {
        return $model
        ->where('name', 'like', "%$query%")
        ->where('nickname', 'like', "%$query%");
    }

    public function scopeWithAll($model)
    {
        return $model;
    }

    public function getPositionNameAttribute() {
        $name = '';
        switch ($this->position_id) {
            case 1:
                $name = 'Goleiro';
                break;
            case 2:
                $name = 'Lateiral';
                break;
            case 3:
                $name = 'Zagueiro';
                break;
            case 4:
                $name = 'Meia';
                break;
            case 5:
                $name = 'Atacante';
                break;
            case 6:
                $name = 'Técnico';
                break;
        }
        return $name;
    }

}
