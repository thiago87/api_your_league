<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\UserPoints;
use App\Users;

class UsersGroups extends Model {

    use SoftDeletes;

    protected $fillable = ['has_paid'];

    protected $dates = [];
    protected $appends = ['total_value', 'position', 'user_name', 'team_name'];
    // protected $eagerload = ["group"];

    protected $casts = [
        'is_admin' => 'boolean',
        'has_paid' => 'boolean',
    ];

    public static $rules = [
        "user_id" => "required",
        "group_id" => "required",
        "is_admin" => "required|boolean",
        "has_paid" => "required|boolean",
    ];

    public function group()
    {
        return $this->belongsTo('App\Groups');
    }

    public function user()
    {
        return $this->belongsTo('App\Users');
    }

    public function getUserNameAttribute() {
        $user = $this->user()->first();
        return $user->name;
    }

    public function getTeamNameAttribute() {
        $user = $this->user()->first();
        return $user->nome_time;
    }

    public function getTotalValueAttribute() {
        $group = $this->group()->first();
        $start = $group->started_session;
        $finish = $group->finished_session;
        $model = "App\UserPoints";
        $total = $model::where('user_id', $this->user_id)->where('is_captain', true)->whereBetween('session', [$start, $finish])->sum('points');
        $total *= 2;
        $total += $model::where('user_id', $this->user_id)->where('is_captain', false)->whereBetween('session', [$start, $finish])->sum('points');
        return $total;
    }

    public function getPositionAttribute() {
        $usersGroups = "App\UsersGroups"::where('group_id', $this->group_id)->where('user_id', '!=', $this->id)->get();
        $usersMorePoint = 1;
        foreach($usersGroups as $key => $value) {
            if ($value->total_value > $this->total_value) {
                $usersMorePoint += 1;
            }
        }
        return $usersMorePoint;
    }

}
