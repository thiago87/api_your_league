<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupAwards extends Model {

    use SoftDeletes;

    protected $fillable = [];

    protected $dates = [];

    // public static $rules = [
    //     "name" => "required|unique:groups",
    // ];

    public function save(array $options = [])
    {
        // before save

        $is_save = parent::save($options);

        // after save code
    }

    // public function scopeWithAll($query)
    // {
    //     $query->with('group_type', 'users');
    // }

    // Relationships
    // public function group_type()
    // {
    //     return $this->belongsTo('App\GroupTypes');
    // }

    // public function users()
    // {
    //     return $this->belongsToMany('App\Users', 'users_groups', 'group_id', 'user_id');
    // }

}
