<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPoints extends Model {

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    protected $casts = [
        'is_captain' => 'boolean',
        'points' => 'float',
        'session' => 'integer',
        'user_id' => 'integer',
        'player_id' => 'integer',
    ];

    public $timestamps = false;

    // Relationships
    public function player()
    {
        return $this->hasOne('App\Players', 'cartola_player_id', 'player_id');
    }


}
